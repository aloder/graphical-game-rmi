package graphics

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color

class DrawCircle(val r: Double) extends Drawable {
  def width:Double = r
  def height:Double = r 
  def draw(gc: GraphicsContext, x: Double, y: Double): Unit = {
    gc.fill = Color.RED
    gc.fillOval(x, y, r, r)
  }
}