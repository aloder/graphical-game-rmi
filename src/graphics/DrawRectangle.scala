package graphics

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint._

/**
 * @author aloder
 */
class DrawRectangle(val x: Double, val y: Double, val width: Double, val height: Double) extends Drawable {
  def draw(gc: GraphicsContext, x: Double, y: Double) = {
    gc.fill = (Color.GRAY)
    gc.fillRect(x, y, width, height)
  }
}