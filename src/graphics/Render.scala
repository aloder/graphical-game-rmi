package graphics

import scalafx.scene.paint.Color
import characters.Character
import utility.InputHandler
import graphicgame.Level
import mapElements.Wall
import graphicgame.PassableLevel
import scalafx.scene.canvas.Canvas
import scalafx.scene.canvas.GraphicsContext
import characters.PassableCharacter
import utility.Circle
import characters.RemotePlayer
import collision.Collider

/**
 * @author aloder
 */
object Render {
  val wallColor: Color = Color.White
  def render(gc: GraphicsContext, rp: RemotePlayer, level: PassableLevel) = {
    val c = new Camera(rp.x, rp.y)
    gc.clearRect(0, 0, gc.canvas.getWidth, gc.canvas.getHeight)
    renderMaze(gc, c.x, c.y, level)

    renderPlayer(gc, rp)
    renderCharacters(gc, level.characters.filterNot { x => x.x == rp.x && x.y == rp.y }, c)
    renderScoreboard(gc, rp)
  }

  def renderMaze(gc: GraphicsContext, cx: Double, cy: Double, level: PassableLevel) = {
    val offsetX: Double = cx - (level.wallWidth / 2) - 400
    val offsetY: Double = cy - (level.wallHeight / 2) - 400
    gc.fill = Color.Black
    gc.fillRect(0, 0, gc.canvas.getWidth + 0.5, gc.canvas.getHeight + 0.5)
    for (e <- level.elems)
      if (e.isDefined) {
        val currentWall = e.get
        gc.fill = wallColor
        gc.fillRect(currentWall.x - (offsetX + currentWall.width / 2), currentWall.y - (offsetY + currentWall.height / 2), currentWall.width + 1, currentWall.height + 1)
      }
  }
  def renderCharacters(gc: GraphicsContext, passC: List[PassableCharacter], c: Camera): Unit = {
    for (char <- passC) {
      renderCharacter(gc, DrawImage.getImage(char.ctype), char, c)
    }
  }
  private def renderCharacter(gc: GraphicsContext, d: Drawable, character: PassableCharacter, c: Camera) = {
    gc.save()
    val drawPosOption = c.getDrawPos(character.x, character.y)
    if (drawPosOption.isDefined) {
      val drawPos = drawPosOption.get
      Circle.rotate(gc, character.r, drawPos._1, drawPos._2)
      d.draw(gc, drawPos._1, drawPos._2)
      gc.fill = Color.Red
      //gc.fillOval(drawPos._1 - 100 / 2, drawPos._2 - 100 / 2, 100, 100)
    }
    gc.restore()
  }
  def renderCollisons(gc: GraphicsContext, passC: List[Collider], c: Camera): Unit = {
    for (col <- passC) {
      gc.save()
      val box = col.getCollisionArea
      val drawPosOption = c.getDrawPos(box.minX, box.minY)
      if (drawPosOption.isDefined) {
        val drawPos = drawPosOption.get
        gc.fillRect(box.minX, box.minY, box.width, box.height)
      }
      gc.restore()
    }
  }
  private def renderPlayer(gc: GraphicsContext, rp: RemotePlayer) = {
    gc.save()
    val d: Drawable = DrawImage.getImage(Character.Player)
    Circle.rotate(gc, rp.getRotation, 400, 400)
    d.draw(gc, 400+d.width/4, 400+d.height/4)
    gc.fill = Color.Red
    //gc.fillOval(400 - 5 / 2, 400 - 5 / 2, 5, 5)
    gc.restore()
  }
  def renderScoreboard(gc: GraphicsContext, rp:RemotePlayer) = {
    gc.fill = Color.Green
    gc.fillText("Score: " + rp.getScore, 10, 10)
    gc.fill = Color.Red
    gc.fillText("Health: " + rp.getHealth, 10, 50)
  }

}