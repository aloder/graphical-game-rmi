package graphics

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.image.Image
import characters.Character
/**
 * @author aloder
 */
class DrawImage(val url: String, val width: Double, val height: Double, val imgRotation: Double = 0) extends Drawable {
  val img = new Image(url, width, height, true, true)
  def draw(gc: GraphicsContext, x: Double, y: Double) = {
    draw(gc,x,y,0)
  }
  override def draw(gc: GraphicsContext, x: Double, y: Double, r: Double) = {
    gc.drawImage(img, x - width / 2, y - height / 2)
  }
}
object DrawImage {
  private val map = scala.collection.mutable.Map[characters.Character.characterType, DrawImage]()
  map += (Character.Player -> new DrawImage("img/spaceships.png", 40, 40))
  map += (Character.Projectile -> new DrawImage("img/playerBullet.png", 40, 40))
  map += (Character.Turret -> new DrawImage("img/turret.png", 40, 40))
  def getImage(c: Character.characterType): DrawImage = {
    map(c)
  }
}