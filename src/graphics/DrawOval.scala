package graphics

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color

class DrawOval(val x: Double, val y: Double, val width: Double, val height: Double) extends Drawable {
  def draw(gc: GraphicsContext, x: Double, y: Double): Unit = {
    gc.fill = Color.RED
    gc.fillOval(x, y, width, height)
  }
}