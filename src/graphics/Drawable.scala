package graphics

import scalafx.scene.canvas.GraphicsContext

/**
 * @author aloder
 */
abstract class Drawable {
  def draw(gc: GraphicsContext, x: Double, y: Double): Unit 
  def draw(gc: GraphicsContext, x: Double, y: Double, r:Double): Unit ={
    draw(gc,x,y)
  }
  def width:Double
  def height:Double
}