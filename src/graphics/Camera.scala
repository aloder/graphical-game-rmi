package graphics

/**
 * @author aloder
 */
case class Camera(val x: Double, val y: Double) {
  def cameraPixelWidth = 800;
  def cameraPixelHeight = 800;
  def seen(x: Double, y: Double): Boolean = {
    val outOfBoundsLeft = x - cameraPixelWidth / 2
    val outOfBoundsRight = x + cameraPixelWidth / 2
    val outOfBoundsTop = y - cameraPixelHeight / 2
    val outOfBoundsDown = y + cameraPixelHeight / 2
    if (x > outOfBoundsLeft && x < outOfBoundsRight) {
      if (y > outOfBoundsTop && y < outOfBoundsDown) {
        return true
      }
    }
    false
  }

  def getDrawPos(x:Double, y:Double): Option[(Double,Double)] = {
    if (seen(x,y)) {
      return Some((x - (this.x - cameraPixelWidth / 2), y - (this.y - cameraPixelHeight / 2)))
    }
    return None
  }
}