package graphicgame

import characters.PassableCharacter
import mapElements.MapElement

/**
 * @author aloder
 */
case class PassableLevel(val style: Int, val elems: List[Option[mapElements.PassableWall]], val characters: List[PassableCharacter]) {
  val wallWidth: Double = 75
  val wallHeight: Double = 75
  val wallDif: Double = 75
  val mazeOffsetx: Double = 400
  val mazeOffsety: Double = 400
}