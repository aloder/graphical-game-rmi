
package graphicgame

import mapElements.Item
import mapElements.MapElement
import characters.Character
import mapElements.Wall
import utility.Maze
import mapElements.Blank
import collision.Collider
import movement.Position2d
import scalafx.geometry.Point2D
import graphics.Render
import clients.RMIClient
import utility.PathFinding
import mapElements.MapElements

/**
 * @author aloder
 */
class Level(val style: Int, private var arrElems: Array[Array[MapElement]], private var mChars: List[Character], private var mItems: List[Item]) {
  private var mElems: List[MapElement] = convertToList(arrElems)
  private var totalUpdates = 0;
  val pathFinder = new utility.PathFinding(this)
  def getPlayer = mChars(mChars.length - 1)
  def maze: List[MapElement] = mElems
  def getMazeArray: Array[Array[MapElement]] = arrElems
  def convertToList(ary: Array[Array[MapElement]]): List[MapElement] = {
    var lst: List[MapElement] = Nil
    for (a <- ary)
      for (b <- a)
        lst ::= b
    lst
  }
  def addCharacter(c: Character): Unit = {
    mChars ::= c
  }
  def addItem(item: Item): Unit = {
    mItems ::= item
  }
  def removeItem(item: Item): Unit = {
    findAndRemove(mItems)(a => a.equals(item))
  }
  def removeCharacter(c: Character): Unit = {
    mChars = findAndRemove(mChars)(a => a.equals(c))._2
  }
  def update = {
    totalUpdates += 1;
    mChars.map { x => x.update }
    collision.CollisionDetector.checkCollisions(mChars, wall)
  }
  def getTotalUpdates = totalUpdates;

  private def findAndRemove[A](lst: List[A])(pred: A => Boolean): (Option[A], List[A]) = {
    lst match {
      case Nil => (None, Nil)
      case h :: t =>
        if (pred(h)) (Some(h), t)
        else {
          val (o, nonMatches) = findAndRemove(t)(pred)
          (o, h :: nonMatches)
        }
    }
  }
  def randomPosition2d: Position2d = {
    var ret: Position2d = null
    while (ret == null) {
      val x = Math.random() * Math.sqrt(mElems.length) * Level.wallWidth
      val y = Math.random() * Math.sqrt(mElems.length) * Level.wallHeight
      val trueness = for (w <- wall) yield {
        if (w.getCollisionArea.contains(new Point2D(x, y))) {
          false
        } else
          true
      }
      if (trueness.min != false)
        ret = new Position2d(x, y)
    }
    ret
  }
  def wall:List[Wall] ={
    var lst:List[Wall] = Nil
    for(e <-mElems)
      e match{
      case w:mapElements.Wall => lst ::= w
      case _ =>
    }
    lst
  }
  def characters: List[Character] = mChars
  def items: List[Item] = mItems
  def getPassable: PassableLevel = new PassableLevel(style, mElems.map { x => x.createPassable }, mChars.map { x => x.createPassable })
}
object Level {
  val wallWidth: Double = 100
  val wallHeight: Double = 100
  /*def makeMazeMapElements(cellSize: Int, wrap: Boolean, numRows: Int, numCols: Int, openFactor: Double): List[MapElement] = {
    val maze = Maze(cellSize, wrap, numRows, numCols, openFactor)
    var lst: List[MapElement] = Nil
    for (r <- 0 until numRows) {
      for (c <- 0 until numCols) {
        if (maze(r, c)) lst ::= new Wall((r * wallWidth), (c * wallHeight), wallWidth, wallHeight)
      }
    }
    lst
  }*/
  def makeMazeMapElements(cellSize: Int, wrap: Boolean, numRows: Int, numCols: Int, openFactor: Double): Array[Array[MapElement]] = {
    val maze = Maze(cellSize, wrap, numRows, numCols, openFactor)
    val arr: Array[Array[MapElement]] = Array.fill(numRows)(Array.fill(numCols)(Blank))
    for (r <- 0 until numRows) {
      for (c <- 0 until numCols) {
        if (maze(r, c)) arr(r)(c) = new Wall((r * wallWidth), (c * wallHeight), wallWidth, wallHeight)
      }
      println()
    }
    arr

  }

}