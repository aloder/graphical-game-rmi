package collision

/**
 * @author aloder
 */
object CollisionDetector {
  def checkCollisions(characters: List[Collider], mapElements: List[Collider]): Unit = {
    var characters2 = characters
    for (c <- characters) {
      characters2 = characters2.tail
      for (c2 <- characters2) {
        if (c.colliding(c2)) {
          c.collided(c2)
          c2.collided(c)
        }
      }
      for (w <- mapElements) {
        if (c.colliding(w)) {
          c.collided(w)
          w.collided(c)
        }
      }
    }
  }
}