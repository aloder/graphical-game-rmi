package collision

import scalafx.geometry.Rectangle2D

/**
 * @author aloder
 */
trait Collider {
  def colliding(c: Collider): Boolean = {
    getCollisionArea.intersects(c.getCollisionArea)
  }
  def |(c: Collider): Boolean = colliding(c)
  def getCollisionArea: Rectangle2D
  def collided(c: Collider): Unit
  def getType:Collider.colliderType
  override def toString():String = {
    "Collider["+ getType+"]: ("+getCollisionArea.toString()+")"
  }
}
object Collider extends Enumeration{
  type colliderType = Value
  val Wall, Blank, Enemy, Player, Projectile, Turret = Value

}