package clients

import java.rmi.Naming
import java.rmi.registry.LocateRegistry
import java.rmi.server.UnicastRemoteObject
import scala.collection.immutable.Map
import characters.Player
import characters.RemotePlayer
import characters.Turret
import graphicgame.Level
import graphicgame.PassableLevel
import movement.Movement2d
import movement.Position2d
import movement.Vector2d
import mapElements.MapElement

/**
 * @author aloder
 */
object RMIServer extends UnicastRemoteObject with RemoteServer {
  private val levels = Array { new Level(0, Level.makeMazeMapElements(2, false, 100, 100, 0.8), Nil, Nil) }
  private var clients = Map[RemoteClient, RemotePlayer]()
  def level(index: Int): Level = levels(index)

  def connect(client: RemoteClient): RemotePlayer = {
    println("Connection")
    val player = new Player(new Movement2d(level(0).randomPosition2d, new Vector2d()), level(0))
    level(0).addCharacter(player)
    clients += (client -> player)
    println("Done Connecting")
    client.updateLevel(level(0).getPassable)
    player
  }

  def update: Unit = {
    level(0).update
    for (k <- clients.iterator) {
      k._1.updateLevel(level(0).getPassable)
    }
  }

  def removeClient(c: RemoteClient): Unit = {
    clients = clients.filter(_ != c)
  }

  def main(args: Array[String]): Unit = {
    LocateRegistry.createRegistry(1099)
    Naming.rebind("GameServer", this)
    for (x <- 0 until 100) level(0).addCharacter(new Turret(new Movement2d(levels(0).randomPosition2d, new Vector2d()), levels(0), 400, 30))
    while (true) {
      update
      Thread.sleep(16)
    }
  }

}