package clients

import java.rmi.Naming
import java.rmi.server.UnicastRemoteObject
import characters.RemotePlayer
import graphicgame.PassableLevel
import graphics.Camera
import mapElements.MapElement
import movement.Movement2d
import movement.Position2d
import movement.Vector2d
import scalafx.Includes._
import scalafx.Includes.jfxKeyEvent2sfx
import scalafx.Includes.jfxMouseEvent2sfx
import scalafx.application.JFXApp
import scalafx.application.Platform
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.control.TextInputDialog
import scalafx.scene.input.KeyCode
import scalafx.scene.input.KeyEvent
import scalafx.scene.input.MouseEvent
import scalafx.scene.layout.BorderPane
import utility.InputHandler
import scalafx.stage.WindowEvent
import javafx.event.EventHandler

/**
 * @author aloder
 */

object RMIClient extends JFXApp {
  val hostInput = new TextInputDialog("localhost")
  hostInput.title = "Server Host"
  hostInput.headerText = "What machine is the server running on?"
  hostInput.contentText = "Host name or IP"
  val host = hostInput.showAndWait() match {
    case Some(s) => s
    case None => sys.exit()
  }
  private val server = Naming.lookup(s"rmi://$host/GameServer") match {
    case rs: RemoteServer => rs
    case _ => throw new IllegalArgumentException("Remote value was not a server.")
  }

  def update(i: InputHandler, rp: RemotePlayer) = {
    if (i.space) rp.spacePressed()
    if (i.W) rp.upPressed()
    if (i.A) rp.leftPressed()
    if (i.S) rp.downPressed()
    if (i.D) rp.rightPressed()
  }

  class ClientImpl extends UnicastRemoteObject with RemoteClient {
    def updateLevel(pl: PassableLevel): Unit = {
      update(input, rp)
      Platform.runLater(graphics.Render.render(canvas.graphicsContext2D, rp, pl))
    }
  }
  val sceneWidth = 800
  val sceneHeight = 800
  private val canvas = new Canvas(sceneWidth, sceneHeight)
  val gc = canvas.graphicsContext2D
  val input: InputHandler = new InputHandler(gc)
  val client = new ClientImpl
  val rp: RemotePlayer = server.connect(client)

  stage = new JFXApp.PrimaryStage {
    title = "Let's Play"
    scene = new Scene(canvas.width.toDouble, canvas.height.toDouble) {

      val centerBorder = new BorderPane

      //Keyboard Events
      canvas.handleEvent(MouseEvent.Any) {
        me: MouseEvent =>
          {
            me.eventType match {
              case MouseEvent.MouseClicked => {
                input.mouseClick_=(true)
              }
              case MouseEvent.MouseMoved => {
                input.mousePosition = new Position2d(me.x.toDouble, me.y.toDouble)
                rp.mouseMoved(me.x.toDouble, me.y.toDouble)
              }
              case _ => {}
            }
          }
      }
      canvas.handleEvent(KeyEvent.KeyPressed) {
        ke: KeyEvent =>
          ke.code match {
            case KeyCode.SPACE => input.space_=(true)
            case KeyCode.W => input.W_=(true)
            case KeyCode.D => input.D_=(true)
            case KeyCode.S => input.S_=(true)
            case KeyCode.A => input.A_=(true)
            case _ => {}
          }

      }
      canvas.handleEvent(KeyEvent.KeyReleased) {
        ke: KeyEvent =>
          ke.code match {
            case KeyCode.SPACE => input.space = false
            case KeyCode.W => input.W_=(false)
            case KeyCode.D => input.D_=(false)
            case KeyCode.S => input.S_=(false)
            case KeyCode.A => input.A_=(false)
            case _ => {}
          }

      }

      //adding to canvas
      centerBorder.center = canvas
      root = centerBorder

      canvas.requestFocus()
    }
  }
  stage.onCloseRequest = (event: WindowEvent) =>{Platform.exit();}
}