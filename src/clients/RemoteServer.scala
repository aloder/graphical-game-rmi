package clients

import graphicgame.PassableLevel
import characters.Player
import graphicgame.Level
import characters.RemotePlayer


/**
 * @author aloder
 */
@remote trait RemoteServer {
  def connect(client: RemoteClient):RemotePlayer
}