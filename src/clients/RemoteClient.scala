package clients

import graphicgame.PassableLevel
import graphics.Camera
import characters.RemotePlayer

/**
 * @author aloder
 */
@remote trait RemoteClient {
  def updateLevel(pl: PassableLevel): Unit
}