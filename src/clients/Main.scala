package clients

/**
 * @author aloder
 */
object Main {
  def main(args: Array[String]): Unit = {
    RMIServer.main(args)
    RMIClient.delayedInit(Thread.sleep(1000))
  }
}