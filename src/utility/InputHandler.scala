package utility

import scalafx.geometry.Point2D
import scalafx.scene.canvas.GraphicsContext
import movement.Position2d

class InputHandler(val gc: GraphicsContext) {
  private var mClicked = false
  private var mPosition = new Position2d(0, 0)
  private var spacePressed = false
  private var wPressed = false
  private var dPressed = false
  private var sPressed = false
  private var aPressed = false
  def mousePosition_=(point: Position2d) {
    mPosition = point
  }
  def mouseClick_=(b: Boolean) {
    mClicked = b
  }
  def space_=(b: Boolean) {
    spacePressed = b
  }
  def W_=(b: Boolean) {
    wPressed = b
  }
  def D_=(b: Boolean) {
    dPressed = b
  }
  def S_=(b: Boolean) {
    sPressed = b
  }
  def A_=(b: Boolean) {
    aPressed = b
  }
  def W = wPressed
  def D = dPressed
  def S = sPressed
  def A = aPressed
  def mousePosition: Position2d = mPosition
  def space = spacePressed
  def mouseClicked = mClicked

}