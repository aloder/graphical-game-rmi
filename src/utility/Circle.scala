package utility

import scalafx.geometry.Point2D
import movement.Rotation
import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.transform.Rotate
import movement.Position2d

object Circle {
  def calculateRotation(p: Position2d, midPoint: Position2d): Rotation = {
    val np = new Point2D(p.x - midPoint.x, p.y - midPoint.y)
    var ang = Math.toDegrees(Math.asin(np.y / Line.length(p, midPoint)))

    if (np.x < 0 && np.y < 0)
      ang = 180 - ang
    else if (np.x < 0)
      ang = 180 - ang
    else if (np.y < 0) {
      ang = ang + 360
    }
    new Rotation(ang)
  }
  def rotate(gc: GraphicsContext, angle: Double, px: Double, py: Double) {
    val r = new Rotate(angle, px, py);
    gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
  }
  
  def calculateRotation2(p: Position2d, midPoint: Position2d): Rotation = {
    val dX = midPoint.x -p.x
    val dY = midPoint.y -p.x
    var ang = Math.toDegrees(Math.atan2(dY, dX))
    new Rotation(ang)
  }
}