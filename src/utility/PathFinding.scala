package utility

import mapElements.MapElement
import movement.Position2d
import scalafx.geometry.Rectangle2D
import mapElements.Wall
import graphicgame.Level
import collision.Collider

/**
 * @author aloder
 */
class PathFinding(val level: Level) {
  def maze = level.getMazeArray

  val wallWidth = maze.head.head.createPassable.get.width
  val wallHeight = maze.head.head.createPassable.get.height
  private val topX = maze.head.head.createPassable.get.x
  private val topY = maze.head.head.createPassable.get.y
  def apply(p1: Position2d, p2: Position2d): List[Position2d] = {
    val p1IntPos = getIntPos(p1.x, p1.y)
    val p2IntPos = getIntPos(p2.x, p2.y)
    breadthFirstShortestPath(p1IntPos._2, p1IntPos._2, p2IntPos._1, p2IntPos._2)._2.map(getDoublePos)
  }
  private def breadthFirstShortestPath(sx: Int, sy: Int, ex: Int, ey: Int): (Int, List[(Int, Int)]) = {
    val offsets = Array((1, 0), (-1, 0), (0, -1), (0, 1))
    val visited = collection.mutable.Set[(Int, Int)]()
    val queue = collection.mutable.Queue[(Int, Int, Int, List[(Int, Int)])]()
    queue.enqueue((sx, sy, 0, List((sx, sy))))
    var answer = -1
    var rList: List[(Int, Int)] = Nil
    if (maze(ex)(ey).getType == Collider.Wall != false) {
      return (-1, Nil)
    }
    if (sx == ex && sy == ex) {
      return (0, Nil)
    }
    while (queue.nonEmpty && answer < 0) {
      val (x, y, steps, l) = queue.dequeue()
      for ((ox, oy) <- offsets) {
        if (x + ox >= 0 && x + ox < maze.length &&
          y + oy >= 0 && y + oy < maze(x).length &&
          maze(x + ox)(y + oy).getType == Collider.Blank && !visited((x + ox, y + oy))) {
          if (x + ox == ex && y + oy == ey) {
            answer = steps + 1
            rList = l.reverse
          }

          queue.enqueue((x + ox, y + oy, steps + 1, (x + ox, y + oy) :: l))
          visited((x + ox, y + oy)) = true
        }
      }
    }
    (answer, rList)
  }
  def getIntPos(x: Double, y: Double): (Int, Int) = {
    var r = (0, 0)
    for (ix <- 0 until maze.length) {
      if (x < ((ix + 1) * wallWidth) - 400 && x > (ix * wallWidth) - 400) r = (ix, r._2)
    }
    for (iy <- 0 until maze.head.length)
      if (y < ((iy + 1) * wallHeight) - 400 && y > (iy * wallHeight) - 400) r = (r._1, iy)
    r
  }
  def getDoublePos(a:(Int,Int)): Position2d = {
    new Position2d((a._1 * wallWidth) - topX + wallWidth / 2, (a._2 * wallHeight) - topY + wallHeight / 2)
  }
}