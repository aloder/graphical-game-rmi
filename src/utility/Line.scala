package utility

import scalafx.geometry.Point2D
import movement.Position2d

object Line {

  def slope(p: Position2d, p2: Position2d): Double = {
    ((p2.y - p.y) / (p2.x - p2.y))
  }
  def length(p: Position2d, p2: Position2d): Double = {
    Math.sqrt(Math.pow(p2.x - p.x, 2) + Math.pow(p2.y - p.y, 2))
  }
}