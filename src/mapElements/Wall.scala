package mapElements

import characters.Character
import collision.Collider
import scalafx.geometry.Rectangle2D

/**
 * @author aloder
 */
class Wall(val x: Double, val y: Double, val width: Double, val height: Double) extends MapElement with collision.Collider {
  def canPass(c: Character): Boolean = {
    false
  }
  def createPassable = Some(new PassableWall(x, y, height, width))
  def getType = Collider.Wall
  def collided(c: Collider): Unit = {}
  def getCollisionArea = new Rectangle2D(x , y, width, height)
}