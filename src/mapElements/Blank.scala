package mapElements

import characters.Character
import collision.Collider

/**
 * @author aloder
 */
object Blank extends MapElement {
  def canPass(c: Character): Boolean = {
    true
  }
  def createPassable = None
  def getType:Collider.Value = Collider.Blank
}