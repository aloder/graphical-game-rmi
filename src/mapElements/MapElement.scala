
package mapElements

import characters.Character
import collision.Collider

/**
 * @author aloder
 */
trait MapElement {
  def canPass(c: Character): Boolean
  def getType:Collider.Value
  def createPassable:Option[PassableWall]
}
