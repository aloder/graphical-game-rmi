package mapElements

/**
 * @author aloder
 */
case class PassableWall(val x:Double, val y:Double,val width:Double,val height:Double)