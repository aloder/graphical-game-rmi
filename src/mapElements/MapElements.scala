package mapElements

/**
 * @author aloder
 */
object MapElements extends Enumeration{
  type characterType = Value
  val Wall, Blank = Value
}