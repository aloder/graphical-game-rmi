

package mapElements

import utility.InputHandler
/**
 * @author aloder
 */
abstract class Item(private var x: Double, private var y: Double) {
  def getX: Double = this.x
  def getY: Double = this.y
  def update(i: InputHandler): Unit
}