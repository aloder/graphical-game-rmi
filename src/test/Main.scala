package test

import movement.Vector2d

/**
 * @author aloder
 */
object Main {
  def main(args: Array[String]): Unit = {
    val vec = new Vector2d(0, 10, 0)
    val vec2 = new Vector2d(10, 10, 180)
    p(vec + vec2)
  }
  def p(s: Any) = {
    println(s)
  }
}