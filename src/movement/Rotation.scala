package movement

/**
 * @author aloder
 */
class Rotation(value: Double) {
  val rotation = clampRotation(value)
  private def clampRotation(value: Double): Double = {
    if (value > 360)
      return clampRotation(value - 360.0)
    if (value < 0)
      return clampRotation(value + 360.0)
    value
  }
  def +(rot: Rotation) = {
    new Rotation(rot.rotation + rotation)
  }
  def |(rot: Rotation) = {
    new Rotation((rot.rotation + rotation) / 2)
  }
  def -(rot: Rotation) = {
    new Rotation(rotation - rot.rotation)
  }
  def *(rot: Rotation): Rotation = {
    new Rotation(rot.rotation * rotation)
  }
  def /(rot: Rotation): Rotation = {
    new Rotation((rotation) / (rot.rotation))
  }
  def unary_-(): Rotation = {
    new Rotation(rotation + 180)
  }
  override def toString: String = {
    "Rotation: " + rotation.toString()
  }
  def rotationRadians: Double = Math.toRadians(rotation)

}