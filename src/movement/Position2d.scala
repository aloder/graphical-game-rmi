

package movement

/**
 * @author aloder
 */
class Position2d(val x: Double, val y: Double, val rotation: Rotation) {
  def this(x: Double, y: Double, r: Double) {
    this(x, y, new Rotation(r))
  }
  def this(x: Double, y: Double) {
    this(x, y, 0)
  }
  def +(pos: Position2d) = new Position2d(x + pos.x, y + pos.y, rotation)
  def -(pos: Position2d) = new Position2d(x - pos.x, y - pos.y, rotation)
  def +(rot: Rotation) = new Position2d(x, y, rotation + rot)
  override def toString(): String = "Position2d(" + x + "," + y + "," + rotation + ")"
}