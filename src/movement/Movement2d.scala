package movement

import scalafx.geometry.Point2D
import utility.Circle

/**
 * @author aloder
 */
class Movement2d(private var pos: Position2d, private var vec: Vector2d) {
  val max = vec.max
  private var midPoint = new Position2d(400, 400)
  def apply(pos: Position2d, vec: Vector2d) = {
    this.pos = pos
    this.vec = vec
  }
  def apply(x: Double, y: Double) = {
    pos = new Position2d(x, y, 0)
    vec = new Vector2d(0, max, new Rotation(0))
  }
  def apply(rot: Rotation) = {
    pos
  }
  def tick(scaler: Double): Unit = {
    pos = pos + new Position2d(scaler * vec.calculateX, scaler * vec.calculateY, pos.rotation)
  }
  def mp_=(p: Position2d) {
    midPoint = p
  }
  def rotation_=(r: Double) {
    pos = new Position2d(pos.x, pos.y, r)
  }
  def rotate(p: Position2d) = {
    val rot = Circle.calculateRotation(p, midPoint)
    rotation_=(rot.rotation)
  }
  def getPlayerRotationDouble: Double = pos.rotation.rotation
  def getPlayerRotation: Rotation = pos.rotation
  def getPosition = pos
  def addVector(vec2: Vector2d) = {
    vec = vec + vec2
  }
  def getVector: Vector2d = vec
  def setVector(v: Vector2d) = vec = v
  def x = pos.x
  def y = pos.y
  override def toString(): String = {
    "Position:" + pos + " Vector:" + vec
  }
}