package movement

import scalafx.scene.canvas.GraphicsContext
import scalafx.scene.paint.Color
import scalafx.scene.transform.Rotate
import scalafx.scene.transform.Rotate.sfxRotate2jfx

/**
 * @author aloder
 */
class Vector2d(mag: Double, val max: Double, val rotation: Rotation) {
  val magnatude = clampMagnatude(mag)
  def this() = {
    this(0, 250, new Rotation(0))
  }
  def this(mag: Double, max: Double, rot: Double) = {
    this(mag, max, new Rotation(rot))
  }
  private def clampMagnatude(value: Double): Double = {
    if (value > max)
      max
    value
  }
  //TODO Actual Collison
  def unary_-(): Vector2d = {
    new Vector2d(mag, max, -rotation)
  }
  def +(vec: Vector2d): Vector2d = {
    val ax = calculateX
    val ay = calculateY
    val bx = vec.calculateX
    val by = vec.calculateY
    val cx = ax + bx
    val cy = ay + by
    val cm = Math.sqrt(Math.pow(cx, 2) + Math.pow(cy, 2))
    val cr = Math.atan2(cy, cx).toDegrees
    new Vector2d(cm, max, new Rotation(cr))
  }
  def calculateX: Double = {
    magnatude * Math.cos(rotation.rotationRadians)
  }
  def calculateY: Double = {
    magnatude * Math.sin(rotation.rotationRadians)
  }

  override def toString: String = {
    "Vector:(mag: " + magnatude.toInt + ", max:" + max.toInt + ", rotation: " + rotation.rotation.toInt + ")"
  }
  def drawVector(gc: GraphicsContext, c: Color) = {
    gc.save()
    val r = new Rotate(rotation.rotation, 400, 400);
    gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    gc.fill = c
    gc.fillRect(399, 399, magnatude * 5, 2)
    gc.restore()
  }

}