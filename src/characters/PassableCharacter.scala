package characters

import movement.Position2d
import graphics.Drawable
import movement.Vector2d
import graphics.DrawImage
import movement.Movement2d
import graphicgame.Level
import movement.Rotation

/**
 * @author aloder
 */
case class PassableCharacter(val x:Double, val y:Double, val r:Double, val ctype:Character.Value)