package characters

/**
 * @author aloder
 */
@remote trait RemotePlayer {
  def upPressed():Unit
  def downPressed():Unit
  def rightPressed():Unit
  def leftPressed():Unit
  def spacePressed():Unit
  def addScore(s:Int):Unit
  def update():Unit
  def mouseMoved(x:Double,y:Double):Unit
  def getRotation:Double
  def x:Double
  def y:Double
  def getScore:Int
  def getHealth:Double
  override def toString():String = "Remote Player:(x:"+x.toInt+" ,y:"+y.toInt+" ,r:"+getRotation.toInt+")"
}