

package characters

import java.rmi.server.UnicastRemoteObject

import graphicgame.Level
import movement.Movement2d
import movement.Position2d
import movement.Vector2d
import scalafx.geometry.Rectangle2D

/**
 * @author aloder
 */
abstract class Character(val position: Movement2d, val collisonArea: Rectangle2D, protected var mLevel: Level) extends UnicastRemoteObject with collision.Collider {
  //values
  protected var health: Double = 100
  val collisonRadius = Math.sqrt(Math.pow(collisonArea.width / 2, 2) + Math.pow(collisonArea.width / 2, 2)) * 1.5

  //get functions
  def pos: Movement2d = position
  def x: Double = position.x
  def y: Double = position.y
  def level: Level = mLevel
  def createPassable: PassableCharacter
  def update: Unit
  //Movement
  def move(mov: Position2d, vec: Vector2d): Unit = {
    position(mov, vec)
  }
  def move(nx: Double, ny: Double, nl: Level): Unit = {
    position(nx, ny)
    mLevel = nl
  }

  def getCollisionArea: Rectangle2D = new Rectangle2D(x - collisonArea.height / 2, y - collisonArea.width / 2, collisonArea.width, collisonArea.height)
  def getType: collision.Collider.colliderType
  def takeDamage(d: Double): Unit = {
    health -= d
    if (isDead)
      handleDeath
  }
  def addHealth(h: Double): Unit = {
    health += h
  }
  def isDead: Boolean = {
    if (health <= 0)
      true
    else
      false
  }
  def handleDeath: Unit = {
    level.removeCharacter(this)
  }
}

object Character extends Enumeration {
  type characterType = Value
  val Player, Enemy, Projectile, Turret = Value
}