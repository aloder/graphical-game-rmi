package characters

import graphicgame.Level
import movement.Movement2d
import collision.Collider
import scalafx.geometry.Rectangle2D
import movement.Position2d
import movement.Rotation
import movement.Vector2d
import utility.Circle

/**
 * @author aloder
 */
class Turret(pos: Movement2d, level: Level, val rangeRadus: Double, val shootingSpeed: Double) extends Enemy(pos, level, new Rectangle2D(0, 0, 20, 20)) {
  var target: Option[Character] = None
  val bulletVelocity: Double = 100
  private var lastTimeShot = 0;
  private var lockOnTime = 0;
  health = 20
  def update(): Unit = {
    findNewTarget
    if (target != None) {
      move(target.getOrElse(null).pos.getPosition)
      shoot
    }
  }
  def shoot: Unit = {
    if (lastTimeShot + shootingSpeed <= level.getTotalUpdates && lockOnTime + shootingSpeed <= level.getTotalUpdates) {
      lastTimeShot = level.getTotalUpdates
      val x = position.x + collisonRadius * Math.cos(position.getPlayerRotation.rotationRadians)
      val y = position.y + collisonRadius * Math.sin(position.getPlayerRotation.rotationRadians)
      val pos = new Position2d(x, y, new Rotation(position.getPlayerRotationDouble - 90))
      val proPos = new Movement2d(pos, new Vector2d(bulletVelocity, 100, position.getPlayerRotationDouble))
      level.addCharacter(new Projectile(proPos, level))
    }
  }
  def move(p: Position2d) = {
    position.rotation_=(Circle.calculateRotation(p, position.getPosition).rotation)
    position.tick(0.1)

  }
  def findNewTarget {
    if (target.isEmpty)
      lockOnTime = level.getTotalUpdates
    target = None
    for (c <- mLevel.characters) {
      if (c.getType != Collider.Turret && c.getType != Collider.Projectile)
        if (seen(c.x, c.y)) {
          target = Some(c)

        }
    }
  }
  def seen(x: Double, y: Double): Boolean = {
    val outOfBoundsLeft = position.x - rangeRadus
    val outOfBoundsRight = position.x + rangeRadus
    val outOfBoundsTop = position.y - rangeRadus
    val outOfBoundsDown = position.y + rangeRadus
    if (x > outOfBoundsLeft && x < outOfBoundsRight) {
      if (y > outOfBoundsTop && y < outOfBoundsDown) {
        return true
      }
    }
    false
  }
  override def collided(c: Collider): Unit = {
    c match {
      case w: mapElements.Wall => position.setVector(-position.getVector);
      case p: characters.Projectile => takeDamage(p.damage);
      case player: characters.Player =>
      case e =>
    }
  }
  def getType = Collider.Turret
  def createPassable: PassableCharacter = new PassableCharacter(pos.getPosition.x, pos.getPosition.y, pos.getPlayerRotationDouble, Character.Turret)
}