package characters

import graphicgame.Level
import graphics.Drawable
import movement.Movement2d
import scalafx.scene.canvas.GraphicsContext
import utility.InputHandler
import utility.Circle
import movement.Vector2d
import movement.Position2d
import scalafx.geometry.Rectangle2D
import collision.Collider

/**
 * @author aloder
 */
class Projectile(pos: Movement2d, l: Level,val owner:Option[Player] = None,val damage: Int = 10) extends Character(pos, new Rectangle2D(0, 0, 10, 10), l) {
  private val creationTick = level.getTotalUpdates
  private val totalTicksAlive = 100
  def update() = {
    position.tick(0.1)
    if (creationTick + totalTicksAlive <= level.getTotalUpdates) {
      level.removeCharacter(this)
    }
  }
  def createPassable = new PassableCharacter(pos.x, pos.y, pos.getPlayerRotationDouble, Character.Projectile)
  def getType = Collider.Projectile
  def collided(c: Collider) = {
    c.getType match {
      case Collider.Wall => level.removeCharacter(this)
      case Collider.Player => level.removeCharacter(this)
      case Collider.Turret => if(owner.isDefined) owner.get.addScore(1); level.removeCharacter(this);
      case Collider.Projectile => level.removeCharacter(this)
      case _ => level.removeCharacter(this)
    }
  }
}