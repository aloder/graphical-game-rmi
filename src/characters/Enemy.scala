package characters

import graphicgame.Level
import graphics.Drawable
import movement.Movement2d
import scalafx.scene.canvas.GraphicsContext
import utility._
import movement.Vector2d
import movement.Position2d
import scalafx.geometry.Rectangle2D
import collision.Collider

/**
 * @author aloder
 */
abstract class Enemy(pos: Movement2d, level: Level, collisionBox: Rectangle2D) extends Character(pos, collisionBox, level) {

  def update(): Unit
  def collided(c: Collider): Unit = {
/*    c match {
      case w: mapElements.Wall => position.setVector(-position.getVector);
      case p: characters.Projectile =>
        takeDamage(p.damage);
      case player: characters.Player =>
        takeDamage(1); position.setVector(-position.getVector);
      case e =>
    }*/
  }
  def getType: Collider.colliderType
  def createPassable: PassableCharacter

}