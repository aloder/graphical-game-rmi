package characters

import graphicgame.Level
import graphics.Drawable
import movement.Movement2d
import movement.Position2d
import movement.Vector2d
import scalafx.scene.canvas.GraphicsContext
import utility.Circle
import utility.InputHandler
import movement.Rotation
import scalafx.geometry.Rectangle2D
import collision.Collider

/**
 * @author aloder
 */
class Player(pos: Movement2d, level: Level) extends Character(pos, new Rectangle2D(0, 0, 20, 20), level) with RemotePlayer {
  val thrust: Double = 0.5
  val bulletVelocity: Double = 100
  val shootingSpeed = 20
  private var score = 0
  private var lastTimeShot = 0;
  println("Press S to see the closest enemy")
  def mouseMoved(x: Double, y: Double): Unit = position.rotate(new Position2d(x, y))
  def shoot: Unit = {
    if (lastTimeShot + shootingSpeed <= level.getTotalUpdates) {
      lastTimeShot = level.getTotalUpdates
      val x = position.x + collisonRadius * Math.cos(position.getPlayerRotation.rotationRadians)
      val y = position.y + collisonRadius * Math.sin(position.getPlayerRotation.rotationRadians)
      val pos = new Position2d(x, y, new Rotation(position.getPlayerRotationDouble - 90))
      val proPos = new Movement2d(pos, new Vector2d(bulletVelocity, 100, position.getPlayerRotationDouble))
      level.addCharacter(new Projectile(proPos, level, Some(this)))
    }
  }
  def move(Rotation: Double) {
    position.addVector(new Vector2d(0.5, 20, Rotation))
  }
  def moveForward: Unit = {
    move(position.getPlayerRotationDouble)
  }
  def moveLeft = {
    move(position.getPlayerRotationDouble - 90)
  }
  def moveRight = {
    move(position.getPlayerRotationDouble + 90)
  }
  def moveBack = {
    //move(position.getPlayerRotationDouble - 180)
    println("Nearest Enemy: " + findNearestEnemy.mkString("\n"))
  }
  def collided(c: Collider): Unit = {
    c match {
      case w: mapElements.Wall => position.setVector(-position.getVector);
      case p: characters.Projectile => takeDamage(p.damage);
      case player: characters.Player => takeDamage(1); position.setVector(-position.getVector);
      case t: characters.Turret => position.setVector(-position.getVector);
      case e =>
    }
  }
  def findNearestEnemy: List[Position2d] = {
    val y = for (c <- level.characters) yield if (c != this || c.getType != Collider.Player)
      level.pathFinder(c.pos.getPosition, this.pos.getPosition)
    else
      Nil
    val f = y.filter { x => x.nonEmpty }
    if(f.isEmpty == false)
      f.foldLeft(f.head)((A, B) => if (A.length < B.length) A else B)
    else
      List[Position2d]()

  }

  def getType = Collider.Player
  def upPressed(): Unit = moveForward
  def downPressed(): Unit = moveBack
  def rightPressed(): Unit = moveRight
  def leftPressed(): Unit = moveLeft
  def spacePressed(): Unit = shoot
  def update(): Unit = position.tick(0.1)
  def getRotation: Double = pos.getPlayerRotationDouble
  def createPassable: PassableCharacter = new PassableCharacter(pos.getPosition.x, pos.getPosition.y, pos.getPlayerRotationDouble, Character.Player)
  def addScore(s: Int) = score += s
  def getScore: Int = score
  def getHealth: Double = health

}
